// Fill out your copyright notice in the Description page of Project Settings.


#include "MyObstaclesSpawn.h"
#include "MyObstacles.h"
#include <random>

// Sets default values
AMyObstaclesSpawn::AMyObstaclesSpawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

void AMyObstaclesSpawn::AddObstacles()
{
	//����� ������� ��� ����� ��������
	FRotator DonRot = FRotator(0, 0, 0);
	//��������� ������� �������� ����� �� ������� ����� ��������� �������
	std::random_device rd;
	std::mt19937 rg(rd());
	auto generator = std::uniform_real_distribution<float>(0, 3);
	for (size_t i = 0; i < 4; ++i) {
		int NumbLine = generator(rg);
		if (NumbLine == 0) {//������� ����� ������
			float SpawnX = FMath::FRandRange(Xobj, Xobj + Width);
			FVector SpawnPoint = FVector(SpawnX, Line_0, SpawnZ);
			if (GetWorld()) {
				GetWorld()->SpawnActor<AMyObstacles>(SpawnPoint, DonRot);
			}
		}
		if (NumbLine == 1) {//������� ����� ������
			float SpawnX = FMath::FRandRange(Xobj, Xobj + Width);
			FVector SpawnPoint = FVector(SpawnX, Line_1, SpawnZ);
			if (GetWorld()) {
				GetWorld()->SpawnActor<AMyObstacles>(SpawnPoint, DonRot);
			}
		}
		if (NumbLine == 2) {//������� ����� ������
			float SpawnX = FMath::FRandRange(Xobj, Xobj + Width);
			FVector SpawnPoint = FVector(SpawnX, Line_2, SpawnZ);
			if (GetWorld()) {
				GetWorld()->SpawnActor<AMyObstacles>(SpawnPoint, DonRot);
			}
		}
	}
	Xobj += Width;
}

// Called when the game starts or when spawned
void AMyObstaclesSpawn::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMyObstaclesSpawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	BuffTimer += DeltaTime;
	if (BuffTimer > Delay) {
		AddObstacles();
		BuffTimer = 0;
	}
}

// Called to bind functionality to input
void AMyObstaclesSpawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

