// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "MyFloorSpawn.generated.h"

UCLASS()
class MYCOURSEPROJECT_API AMyFloorSpawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AMyFloorSpawn();
	//����� �� ������� ����� ���������� �������.
	float Line_0 = 0.f, Line_1 = 500.f, Line_2 = -500.f;
	//����� ��������
	float Delay = 2.20f;
	//�������
	float BuffTimer = 0.f;
	//����� ������������ � ����������� ������ ��� ��������.
	float SpawnZ = 160.f;
	float WallZ = 560.f;
	float WallY = 950.f;
	//����� ���������� ���������� �� ����������� �������� �� ��� Ox
	float X = 1110;
	float Width = 2200;
	//������� ���������� ���������� ��������
	void AddFloor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
