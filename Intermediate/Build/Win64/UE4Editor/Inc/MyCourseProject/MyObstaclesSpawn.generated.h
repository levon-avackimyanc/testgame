// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MYCOURSEPROJECT_MyObstaclesSpawn_generated_h
#error "MyObstaclesSpawn.generated.h already included, missing '#pragma once' in MyObstaclesSpawn.h"
#endif
#define MYCOURSEPROJECT_MyObstaclesSpawn_generated_h

#define MyCourseProject_Source_MyCourseProject_MyObstaclesSpawn_h_12_SPARSE_DATA
#define MyCourseProject_Source_MyCourseProject_MyObstaclesSpawn_h_12_RPC_WRAPPERS
#define MyCourseProject_Source_MyCourseProject_MyObstaclesSpawn_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define MyCourseProject_Source_MyCourseProject_MyObstaclesSpawn_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMyObstaclesSpawn(); \
	friend struct Z_Construct_UClass_AMyObstaclesSpawn_Statics; \
public: \
	DECLARE_CLASS(AMyObstaclesSpawn, APawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MyCourseProject"), NO_API) \
	DECLARE_SERIALIZER(AMyObstaclesSpawn)


#define MyCourseProject_Source_MyCourseProject_MyObstaclesSpawn_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAMyObstaclesSpawn(); \
	friend struct Z_Construct_UClass_AMyObstaclesSpawn_Statics; \
public: \
	DECLARE_CLASS(AMyObstaclesSpawn, APawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MyCourseProject"), NO_API) \
	DECLARE_SERIALIZER(AMyObstaclesSpawn)


#define MyCourseProject_Source_MyCourseProject_MyObstaclesSpawn_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMyObstaclesSpawn(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMyObstaclesSpawn) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyObstaclesSpawn); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyObstaclesSpawn); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyObstaclesSpawn(AMyObstaclesSpawn&&); \
	NO_API AMyObstaclesSpawn(const AMyObstaclesSpawn&); \
public:


#define MyCourseProject_Source_MyCourseProject_MyObstaclesSpawn_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyObstaclesSpawn(AMyObstaclesSpawn&&); \
	NO_API AMyObstaclesSpawn(const AMyObstaclesSpawn&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyObstaclesSpawn); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyObstaclesSpawn); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMyObstaclesSpawn)


#define MyCourseProject_Source_MyCourseProject_MyObstaclesSpawn_h_12_PRIVATE_PROPERTY_OFFSET
#define MyCourseProject_Source_MyCourseProject_MyObstaclesSpawn_h_9_PROLOG
#define MyCourseProject_Source_MyCourseProject_MyObstaclesSpawn_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyCourseProject_Source_MyCourseProject_MyObstaclesSpawn_h_12_PRIVATE_PROPERTY_OFFSET \
	MyCourseProject_Source_MyCourseProject_MyObstaclesSpawn_h_12_SPARSE_DATA \
	MyCourseProject_Source_MyCourseProject_MyObstaclesSpawn_h_12_RPC_WRAPPERS \
	MyCourseProject_Source_MyCourseProject_MyObstaclesSpawn_h_12_INCLASS \
	MyCourseProject_Source_MyCourseProject_MyObstaclesSpawn_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MyCourseProject_Source_MyCourseProject_MyObstaclesSpawn_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyCourseProject_Source_MyCourseProject_MyObstaclesSpawn_h_12_PRIVATE_PROPERTY_OFFSET \
	MyCourseProject_Source_MyCourseProject_MyObstaclesSpawn_h_12_SPARSE_DATA \
	MyCourseProject_Source_MyCourseProject_MyObstaclesSpawn_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	MyCourseProject_Source_MyCourseProject_MyObstaclesSpawn_h_12_INCLASS_NO_PURE_DECLS \
	MyCourseProject_Source_MyCourseProject_MyObstaclesSpawn_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MYCOURSEPROJECT_API UClass* StaticClass<class AMyObstaclesSpawn>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MyCourseProject_Source_MyCourseProject_MyObstaclesSpawn_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
